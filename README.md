# daverona/compose/nextcloud

## Prerequisites

* `nextcloud.example` mapped to `127.0.0.1` &mdash; check [this](https://gitlab.com/daverona/compose/dnsmasq) out
* [Traefik](https://docs.traefik.io/) &mdash; check [this](http://gitlab.com/daverona/compose/traefik) out

## Quick Start

```bash
cp .env.example .env
# edit .env
docker-compose up --detach
```

Wait until nextcloud is up and visit [http://nextcloud.example/](http://nextcloud.example/).
Note that to change database, click *Storage & database* dropdown button.

## APIs

* Provisioning API: [https://docs.nextcloud.com/server/latest/admin\_manual/configuration\_user/user\_provisioning\_api.html](https://docs.nextcloud.com/server/latest/admin_manual/configuration_user/user_provisioning_api.html)
* WebDAV API:  [https://docs.nextcloud.com/server/latest/developer\_manual/client\_apis/WebDAV/index.html](https://docs.nextcloud.com/server/latest/developer_manual/client_apis/WebDAV/index.html)
* Share API: [https://docs.nextcloud.com/server/latest/developer\_manual/client\_apis/OCS/index.html](https://docs.nextcloud.com/server/latest/developer_manual/client_apis/OCS/index.html)
* Activity: [https://github.com/nextcloud/activity](https://github.com/nextcloud/activity)
* Notifications: [https://github.com/nextcloud/notifications/](https://github.com/nextcloud/notifications/)
* Group Folders: [https://github.com/nextcloud/groupfolders](https://github.com/nextcloud/groupfolders)

## References

* Nextcloud admin manual: [https://docs.nextcloud.com/server/latest/admin\_manual/contents.html](https://docs.nextcloud.com/server/latest/admin_manual/contents.html)
* Nextcloud configuration: [https://docs.nextcloud.com/server/latest/admin\_manual/configuration\_server/index.html](https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/index.html)
* EnterpriseyIntranet/nextcloud-API: [https://github.com/EnterpriseyIntranet/nextcloud-API](https://github.com/EnterpriseyIntranet/nextcloud-API)
* Providing default files: [https://docs.nextcloud.com/server/latest/admin\_manual/configuration\_files/default\_files\_configuration.html?highlight=providing](https://docs.nextcloud.com/server/latest/admin_manual/configuration_files/default_files_configuration.html?highlight=providing)
* Nextcloud repository: [https://github.com/nextcloud/server](https://github.com/nextcloud/server)
* Nextcloud registry: [https://hub.docker.com/\_/nextcloud](https://hub.docker.com/_/nextcloud)
